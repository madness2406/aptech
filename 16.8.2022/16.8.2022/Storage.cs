﻿using _16._8._2022;
using OnlineBookShop.BookShop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookShop
{
    namespace StoreShop
    {

        public class Storage : IStorage
        {
            private int count;
            public Book[] bookStore;
            public Storage()
            {
                count = 0;
                bookStore = new Book[100];
            }
            public Book this[int index]
            {
                get
                {
                    return bookStore[index];
                }
                set
                {
                    bookStore[index] = value;
                }
            }
            public int Count
            {
                get => count;
                set
                {
                    count = value;
                }
            }

            public void AddANewBook()
            {
                Book theBook = new Book();
                Console.Write("Name: ");
                string name = Console.ReadLine();
                Console.Write("Author Name: ");
                string authorName = Console.ReadLine();
                Console.Write("Subject: ");
                string subject = Console.ReadLine();
                Console.Write("Buying Price: ");
                double price;
                while (!double.TryParse(Console.ReadLine(), out price))
                {
                    Console.Write("Wrong type input, try again!");
                }

                theBook.SetDetail(name, authorName, subject, price);
                bookStore[count] = theBook;
                count++;
            }

            public bool IsBook(string id)
            {
                var book = bookStore.FirstOrDefault(b => b.Id == id);
                if (book == null)
                {
                    return false;
                }

                return true;
            }

            public void RemoveABook(string id)
            {
                var book = bookStore.FirstOrDefault(b => b.Id == id);
                if (book == null)
                {
                    Console.WriteLine($"Not found book with id = {id}!");
                }
                else
                {
                    bookStore = bookStore.Where(b => b.Id != id).ToArray();
                    Console.WriteLine("Delete successfull");
                }
            }
        }
    }
}
