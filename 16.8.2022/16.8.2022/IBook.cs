﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookShop
{
    namespace BookShop
    {
        public interface IBook
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string AuthorName { get; set; }
            public string Subject { get; set; }
            public double BuyingPrice { get; set; }
            public double SellingPrice { get; set; }
            void ShowDetail();
            void SetDetail(string name, string authorName, string subject, double buyingPrice);
            void SetSellingPrice(double newSellingPrice);
        }
    }
}
