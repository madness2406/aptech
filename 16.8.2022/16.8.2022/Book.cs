﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBookShop
{
    namespace BookShop
    {

        public class Book : IBook
        {
            private static int count = 0;
            private string id;
            private string name;
            private string authorName;
            private string subject;
            private double buyingPrice;
            private double sellingPrice;


            public string Id
            {
                get => id;
                set
                {
                    id = value;
                }
            }
            public string Name
            {
                get => name;
                set
                {
                    name = value;
                }
            }
            public string AuthorName
            {
                get => authorName;
                set
                {
                    authorName = value;
                }
            }
            public string Subject
            {
                get => subject;
                set
                {
                    subject = value;
                }
            }
            public double BuyingPrice
            {
                get => buyingPrice;
                set
                {
                    buyingPrice = value;
                }
            }
            public double SellingPrice
            {
                get => sellingPrice;
                set
                {
                    sellingPrice = value;
                }
            }

            public Book()
            {
                count++;
                Id = "B00" + count;
            }

            public void SetDetail(string name, string authorName, string subject, double buyingPrice)
            {
                Name = name;
                AuthorName = authorName;
                Subject = subject;
                BuyingPrice = buyingPrice;
                SellingPrice = buyingPrice * 1.3;
            }

            public void SetSellingPrice(double newSellingPrice)
            {
                SellingPrice = newSellingPrice;
            }

            public void ShowDetail()
            {
                Console.WriteLine("Name: " + Name);
                Console.WriteLine("Author Name: " + AuthorName);
                Console.WriteLine("Subject: " + Subject);
                Console.WriteLine("Buying Price: " + BuyingPrice);
            }
        }
    }
}
