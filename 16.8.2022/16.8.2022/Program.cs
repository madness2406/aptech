﻿using System;

namespace _16._8._2022
{
    internal class Program
    {
        static void Main(string[] args)
        {
            OnlineShopBook bookShop = new OnlineShopBook();
            int chon;
            do
            {
                Console.WriteLine("1. Admin");
                Console.WriteLine("2. Custom");
                Console.WriteLine("3. Exit");
                while (!int.TryParse(Console.ReadLine(), out chon))
                {
                    Console.WriteLine("Wrong input type, try again!");
                }

                switch (chon)
                {
                    case 1:
                        int chonAdmin;
                        do
                        {
                            Console.WriteLine("1. Import book to store");
                            Console.WriteLine("2. Show the store in detail");
                            Console.WriteLine("3. Change a book selling price");
                            Console.WriteLine("4. Show profits");
                            Console.WriteLine("5. Back");
                            while (!int.TryParse(Console.ReadLine(), out chonAdmin))
                            {
                                Console.WriteLine("Wrong input type, try again!");
                            }

                            switch (chonAdmin)
                            {
                                case 1:
                                    bookShop.ImportBook();
                                    break;
                                case 2:
                                    bookShop.Show();
                                    break;
                                case 3:
                                    bookShop.ChangeSellingPrice();
                                    break;
                                case 4:
                                    Console.WriteLine("Profits: " + bookShop.profits);
                                    break;
                                case 5:
                                    break;
                            }

                        } while (chonAdmin != 5);

                        break;
                    case 2:
                        int chonCustomer;
                        do
                        {
                            Console.WriteLine("1. Show all book of store");
                            Console.WriteLine("2. Buy a book");
                            Console.WriteLine("3. Back");
                            while (!int.TryParse(Console.ReadLine(), out chonCustomer))
                            {
                                Console.WriteLine("Wrong input type, try again!");
                            }

                            switch (chonCustomer)
                            {
                                case 1:
                                    bookShop.Show();
                                    break;
                                case 2:
                                    bookShop.SellABook();
                                    break;
                                case 3:
                                    break;
                            }

                        } while (chonCustomer != 3);

                        break;
                    default:
                        break;
                }

            } while (chon != 3);
        }
    }
}
