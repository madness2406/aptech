﻿using OnlineBookShop.StoreShop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16._8._2022
{
    public class OnlineShopBook
    {
        private Storage BookStorage = new Storage();
        public double profits = 0;
        public void ImportBook()
        {
            Console.Write($"Input n (n <= {100 - BookStorage.Count}): ");
            int n;
            while (!int.TryParse(Console.ReadLine(),out n))
            {
                Console.WriteLine("Wrong type, try again!");
            }
            for (int i = BookStorage.Count - 1; i < n; i++)
            {
                BookStorage.AddANewBook();
            }
        }
        public void Show()
        {
            foreach (var item in BookStorage.bookStore)
            {
                item.ShowDetail();
                Console.WriteLine("_______________________");
            }
        }
        public void SellABook()
        {
            Show();
            Console.Write("Input book ID to buy: ");
            string id = Console.ReadLine();
            var book = BookStorage.bookStore.FirstOrDefault(b => b.Id == id);
            if (BookStorage.IsBook(id))
            {
                profits += book.SellingPrice - book.BuyingPrice;
                BookStorage.RemoveABook(id);
            }
            else
            {
                Console.WriteLine($"Not found the book with id = {id}");
            }
        }
        public void ChangeSellingPrice()
        {
            Console.Write("Input book Id to change selling price: ");
            string id = Console.ReadLine();
            if (BookStorage.IsBook(id))
            {
                double price;
                Console.WriteLine("Input selling price: ");
                while (!double.TryParse(Console.ReadLine(),out price))
                {
                    Console.WriteLine("Wrong input type, try again!");
                }
                for (int i = 0; i < BookStorage.Count; i++)
                {
                    if (BookStorage[i].Id == id)
                    {
                        BookStorage[i].SetSellingPrice(price);
                        break;
                    }
                }
            }
        }
    }
}
