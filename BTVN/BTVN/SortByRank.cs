﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTVN
{
    public class SortByRank : IComparer
    {
        public int Compare(object x, object y)
        {
            IDoctor a = (IDoctor)x;
            IDoctor b = (IDoctor)y;
            return a.Rank.CompareTo(b.Rank);
        }
    }
}
