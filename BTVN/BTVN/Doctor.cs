﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTVN
{
    public class Doctor : IDoctor
    {
        private static int id = 0;
        private string name;
        private DateTime birthDay;
        private string speciality;
        private string email;
        private int rank;
        private readonly string[] PhoneList = new string[3];
        public string this[int index]
        {
            get
            {
                return PhoneList[index];
            }
            set 
            {
                PhoneList[index] = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public string Name {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public DateTime BirthDay {
            get
            {
                return birthDay;
            }
            set
            {
                birthDay = value;
            }
        }
        public string Speciality {
            get
            {
                return speciality;
            }
            set
            {
                speciality = value;
            }
        }
        public string Email {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public int Rank {
            get
            {
                return rank;
            }
            set
            {
                rank = value;
            }
        }

        public Doctor()
        {
            Id++;
        }

        public void ShowInfo()
        {
            Console.WriteLine("ID: " + Id);
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("BirthDay: " + BirthDay);
            Console.WriteLine("Speciality: " + Speciality);
            Console.WriteLine("Email: " + Email);
        }
    }
}
