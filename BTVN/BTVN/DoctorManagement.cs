﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTVN
{
    public class DoctorManagement
    {
        private ArrayList doctorList = new ArrayList();


        public void Menu()
        {
            Console.WriteLine("PLEASE AN OPTION:");
            Console.WriteLine("1. Add new Doctor");
            Console.WriteLine("2. View list of Doctor");
            Console.WriteLine("3. Sort Doctor by Rank");
            Console.WriteLine("4. Delete a Doctor");
            Console.WriteLine("5. Search Doctor By Email");
            Console.WriteLine("6. Exit");
        }

        public void Add(IDoctor doctor)
        {
            doctorList.Add(doctor);
        }

        public void Sort(IComparer comp)
        {
            doctorList.Sort(comp);
        }

        public void Remove(IDoctor doctor)
        {
            doctorList.Remove(doctor);
        }

        public void MainMenu()
        {
            int chon;
            do
            {
                Menu();
                Console.Write("Your choice: ");
                while (!int.TryParse(Console.ReadLine(), out chon))
                {
                    Console.WriteLine("Wrong, try again!");
                }
                switch (chon)
                {
                    case 1:
                        Console.WriteLine("Add new Doctor");
                        AddNewDoctor();
                        break;
                    case 2:
                        Console.WriteLine("View a list of Doctor");
                        Display();
                        break;
                    case 3:
                        Console.WriteLine("Sort Doctor by Rank");
                        SortByRankDoctor();
                        break;
                    case 4:
                        Console.WriteLine("Delete a Doctor");
                        DeleteDoctor();
                        break;
                    case 5:
                        Console.WriteLine("Search Doctor By Email");
                        SearchDoctorByEmail();
                        break;
                    case 6:
                        break;

                    default:
                        break;
                }

            } while (chon != 6);
        }

        private void SearchDoctorByEmail()
        {
            Console.Write("Input Email: ");
            string email = Console.ReadLine();
            int count = 0;
            foreach (Doctor item in doctorList)
            {
                if (item.Email.Contains(email))
                {
                    count++;
                    item.ShowInfo();
                }
            }
            if (count == 0) Console.WriteLine($"Not Found Doctor With Email: {email}");
        }

        private void DeleteDoctor()
        {
            int id;
            Console.Write("Input Doctor Id to delete: ");
            while (!int.TryParse(Console.ReadLine(),out id))
            {
                Console.WriteLine("Wrong, try again!");
            }
            int count = 0;
            foreach (Doctor item in doctorList)
            {
                if (item.Id == id)
                {
                    count++;
                    Remove(item);
                    Console.WriteLine("Delete successfully!");
                    break;
                }
            }
            if (count == 0) Console.WriteLine($"Not Found Doctor With ID = {id} To Delete!");
        }

        private void SortByRankDoctor()
        {
            for (int i = 0; i < doctorList.Count; i++)
            {
                Sort(new SortByRank());
            }
        }

        private void Display()
        {
            foreach (Doctor item in doctorList)
            {
                item.ShowInfo();
                Console.WriteLine("_____________________________");
            }
        }

        private void AddNewDoctor()
        {
            IDoctor doctor = new Doctor();
            Console.Write("Name: ");
            doctor.Name = Console.ReadLine();
            Console.Write("BirthDay: ");
            DateTime dt;
            while (!DateTime.TryParse(Console.ReadLine(), out dt))
            {
                Console.WriteLine("Wrong, Try again!");
            }
            doctor.BirthDay = dt;
            Console.Write("Speciality: ");
            doctor.Speciality = Console.ReadLine();
            Console.Write("Email: ");
            doctor.Email = Console.ReadLine();
            Console.Write("Rank: ");
            int rank;
            while (!int.TryParse(Console.ReadLine(), out rank))
            {
                Console.WriteLine("Wrong, Try again!");
            }
            doctor.Rank = rank;
            Console.WriteLine("3 Phone number:");
            for (int i = 0; i < 3; i++)
            {
                doctor[i] = Console.ReadLine();
            }
            Add(doctor);
        }
    }
}
