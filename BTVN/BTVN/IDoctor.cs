﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTVN
{
    public interface IDoctor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDay { get; set; }
        public string Speciality { get; set; }
        public string Email { get; set; }
        public int Rank { get; set; }
        string this[int index] { get; set; }
        void ShowInfo();
    }
}
